import 'package:flutter/material.dart';

void main() {
  runApp(const MyFristApp());
}

class MyFristApp extends StatelessWidget {
  const MyFristApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.ac_unit_rounded),
          title: Text('My App'),
          actions: [
            IconButton(
              onPressed: (){},
              icon: Icon(Icons.access_alarm)),
            IconButton(
                onPressed: (){},
                icon: Icon(Icons.abc_sharp))
          ],
        ) ,
        body: Center(
          child: Column(
            children: [
              Text('CHAYANON MUADSONG' ,
                  style: TextStyle(color:Colors.red,
                  fontWeight:FontWeight.bold,fontSize: 20,)),
              Text('6350110002' ,
                  style: TextStyle(color:Colors.pink,
                  fontWeight:FontWeight.w300,fontSize: 18)),
              // Image.asset('assets/boy.jpg'
              // ,width: 350, height: 350, ),
              CircleAvatar(
                backgroundImage: AssetImage('assets/boy.jpg'),radius: 150,
              )
            ],
          ),
        )
      ),
    );
  }
}